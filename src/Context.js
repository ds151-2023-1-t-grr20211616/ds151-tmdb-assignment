import React, { createContext, useReducer } from 'react';

const initialState = {
  notes: []
};

export const NotesContext = createContext(initialState);

const reducer = (state, action) => {
  switch (action.type) {
    case 'ADD_NOTE':
      return {
        ...state,
        notes: [...state.notes, action.payload]
      };
    case 'UPDATE_NOTE':
      const updatedNotes = state.notes.map(note => {
        if (note.id === action.payload.id) {
          return { ...note, ...action.payload };
        }
        return note;
      });
      return {
        ...state,
        notes: updatedNotes
      };
    case 'DELETE_NOTE':
      const filteredNotes = state.notes.filter(note => note.id !== action.payload);
      return {
        ...state,
        notes: filteredNotes
      };
    default:
      return state;
  }
};

export const NotesProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const handleRemoveNote = id => {
    const noteToRemove = state.notes.find(note => note.id === id);
    if (!noteToRemove) {
      return;
    }
    dispatch({ type: 'DELETE_NOTE', payload: id });
  };

  return (
    <NotesContext.Provider value={{ state, dispatch, handleRemoveNote }}>
      {children}
    </NotesContext.Provider>
  );
};
