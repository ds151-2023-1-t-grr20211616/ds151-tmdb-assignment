import React, { useState, useContext } from 'react';
import { View } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import { NotesContext } from '../Context';

const EditNoteScreen = ({ route, navigation }) => {
  const { state, dispatch } = useContext(NotesContext);
  const [title, setTitle] = useState(route.params.note.title);
  const [content, setContent] = useState(route.params.note.content);

  const handleSaveNote = () => {
  const updatedNote = {
  ...route.params.note,
  title,
  content
  };
  dispatch({ type: 'UPDATE_NOTE', payload: updatedNote });
  navigation.goBack();
  };
  
  return (
  <View style={{ flex: 1, padding: 10 }}>
  <TextInput
  label="Título"
  value={title}
  onChangeText={setTitle}
  style={{ marginBottom: 10 }}
  />
  <TextInput
  label="Conteúdo"
  value={content}
  onChangeText={setContent}
  multiline
  style={{ marginBottom: 10 }}
  />
  <Button mode="contained" onPress={handleSaveNote}>
  Salvar
  </Button>
  </View>
  );
  };
  
  export default EditNoteScreen;
