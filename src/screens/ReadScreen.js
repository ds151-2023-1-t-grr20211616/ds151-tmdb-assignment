import React, { useContext } from 'react';
import { View, FlatList } from 'react-native';
import { Button, List } from 'react-native-paper';
import { NotesContext } from '../Context';

const ListNotesScreen = ({ navigation }) => {
  const { state, dispatch } = useContext(NotesContext);

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={state.notes}
        renderItem={({ item }) => (
          <List.Item
            title={item.title}
            description={item.content}
            onPress={() => navigation.navigate('ViewNote', { noteId: item.id })}
          />
        )}
        keyExtractor={item => item.id.toString()}
      />
      <Button mode="contained" onPress={() => navigation.navigate('AddNote')}>
        Adicionar Anotação
      </Button>
    </View>
  );
};

export default ListNotesScreen;
