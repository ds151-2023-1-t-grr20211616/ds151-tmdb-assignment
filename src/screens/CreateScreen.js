import React, { useState, useContext } from 'react';
import { View } from 'react-native';
import { Button, TextInput } from 'react-native-paper';
import { NotesContext } from '../Context';

const AddNoteScreen = ({ navigation }) => {
  const { dispatch } = useContext(NotesContext);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  const handleSaveNote = () => {
    const newNote = {
      id: Date.now(),
      title,
      content
    };
    dispatch({ type: 'ADD_NOTE', payload: newNote });
    navigation.goBack();
  };

  return (
    <View style={{ flex: 1, padding: 10 }}>
      <TextInput
        label="Título"
        value={title}
        onChangeText={setTitle}
        style={{ marginBottom: 10 }}
      />
      <TextInput
        label="Conteúdo"
        value={content}
        onChangeText={setContent}
        multiline
        style={{ marginBottom: 10 }}
      />
      <Button mode="contained" onPress={handleSaveNote}>
        Salvar
      </Button>
    </View>
  );
};

export default AddNoteScreen;
