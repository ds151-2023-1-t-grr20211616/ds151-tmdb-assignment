import React, { useContext } from 'react';
import { View } from 'react-native';
import { Button, Text } from 'react-native-paper';
import { NotesContext } from '../Context';

const ViewNoteScreen = ({ route, navigation }) => {
const { state, dispatch } = useContext(NotesContext);
const note = state.notes.find(note => note.id === route.params.noteId);

const handleEditNote = () => {
navigation.navigate('EditNote', { note });
};

const handleDeleteNote = () => {
dispatch({ type: 'DELETE_NOTE', payload: note.id });
navigation.goBack();
};

return (
  <View style={{ flex: 1, padding: 10 }}>
    <Text style={{ fontSize: 24, fontWeight: 'bold', marginBottom: 10 }}>
      {note?.title}
    </Text>
    <Text style={{ fontSize: 16 }}>{note?.content}</Text>
    <Button mode="contained" onPress={handleEditNote} style={{ marginTop: 10 }}>
      Editar
    </Button>
    <Button mode="outlined" onPress={handleDeleteNote} style={{ marginTop: 10 }}>
      Remover
    </Button>
  </View>
);

};

export default ViewNoteScreen;