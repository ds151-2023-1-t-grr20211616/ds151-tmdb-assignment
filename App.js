import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider as PaperProvider } from 'react-native-paper';
import { NotesProvider } from './src/Context';
import ListNotesScreen from './src/screens/ReadScreen';
import AddNoteScreen from './src/screens/CreateScreen';
import EditNoteScreen from './src/screens/UpdateScreen';
import ViewNoteScreen from './src/screens/ViewScreen';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NotesProvider>
      <PaperProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="ListNotes">
            <Stack.Screen name="ListNotes" component={ListNotesScreen} options={{ title: 'Anotações' }} />
            <Stack.Screen name="AddNote" component={AddNoteScreen} options={{ title: 'Nova Anotação' }} />
            <Stack.Screen name="EditNote" component={EditNoteScreen} options={{ title: 'Editar Anotação' }} />
            <Stack.Screen name="ViewNote" component={ViewNoteScreen} options={{ title: 'Detalhes da Anotação' }} />
          </Stack.Navigator>
        </NavigationContainer>
      </PaperProvider>
    </NotesProvider>
  );
}
